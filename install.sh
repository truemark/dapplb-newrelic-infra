#!/usr/bin/env bash

set -eou pipefail

[[ -z "${NEWRELIC_LICENSE_KEY+z}" ]] && >&2 echo "NEWRELIC_LICENSE_KEY is required" && exit 1
[[ -z "${DAPPL_OS_TYPE+z}" ]] && >&2 echo "DAPPL_OS_TYPE is required" && exit 1
[[ "${DAPPL_OS_TYPE}" != "linux" ]] && >&2 echo "OS type is not linux" && exit 1
[[ -z "${DAPPL_OS_ARCH+z}" ]] && >&2 echo "DAPPL_OS_ARCH is required" && exit 1
[[ "${DAPPL_OS_ARCH}" != "amd64" ]] && >&2 echo "architecture is not amd64" && exit 1
[[ -z "${DAPPL_OS_DISTRO+z}" ]] && >&2 echo "DAPPL_OS_DISTRO is required" && exit 1
[[ $EUID -ne 0 ]] && >&2 echo "must be run as root user" && exit 1

function install_apt() {
  export DEBIAN_FRONTEND="noninteractive"
  [[ -z "${DAPPL_OS_CODENAME+z}" ]] && >&2 echo "DAPPL_OS_CODENAME is required" && exit 1
  apt-get update -qq && apt-get install -qq curl gnupg
  curl -sSL https://download.newrelic.com/infrastructure_agent/gpg/newrelic-infra.gpg | apt-key add -
  echo "deb [arch=amd64] https://download.newrelic.com/infrastructure_agent/linux/apt ${DAPPL_OS_CODENAME} main" \
    > /etc/apt/sources.list.d/newrelic-infra.list
  echo "license_key: ${NEWRELIC_LICENSE_KEY}" | tee -a /etc/newrelic-infra.yml
  apt-get update -qq && apt-get install -qq newrelic-infra
}

function install_yum() {
  [[ -z "${DAPPL_OS_VERSION}" ]] && >&2 echo "DAPPL_OS_VERSION is required" && exit 1
  yum -y -q install curl
  curl -sSL "https://download.newrelic.com/infrastructure_agent/linux/yum/el/${DAPPL_OS_VERSION/.*}/x86_64/newrelic-infra.repo" \
    -o "/etc/yum.repos.d/newrelic-infra.repo"
  yum -q makecache -y --disablerepo='*' --enablerepo='newrelic-infra'
  echo "license_key: ${NEWRELIC_LICENSE_KEY}" | tee -a /etc/newrelic-infra.yml
  yum install -y -q newrelic-infra
}

case "${DAPPL_OS_DISTRO}" in
  debian) install_apt ;;
  ubuntu) install_apt ;;
  centos) install_yum ;;
  ol) install_yum ;;
  *) >&2 echo "Unsupported distribution ${DAPPL_OS_DISTRO}" && exit 1
esac
