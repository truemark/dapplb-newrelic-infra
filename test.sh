#!/usr/bin/env bash

cd "$(dirname "${0}")" || exit 1

vagrant ssh ubuntu -c "bash -s" <<-EOF
  curl -fs https://download.truemark.io/dapplb/install.sh | sudo bash -s apply '/vagrant/test-config.json'
EOF
