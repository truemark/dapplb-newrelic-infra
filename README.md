# NewRelic Infrastructure DAPPL Bootstrap

This is a basic DAPPL bootstrap role used to install the NewRelic infrastructure agent.

## Local Development

This project uses [Vagrant](https://www.vagrantup.com/docs/installation)

Startup machines and create snapshots
```bash
vagrant up
vagrant status
vagrant snapshot push
```

Run tests
```bash
./test.sh
```

Revert snapshot to repeat testing
```bash
vagrant snapshot pop --no-delete
```

Destroy machines when done
```bash
vagrant destroy
```
